﻿
using System;
using System.Collections.Generic;

namespace OhmCalculator.Models
{
    public class OhmValueCalculator: IOhmValueCalculator
    {
        public List<ResistorBand> ResistorBands = new List<ResistorBand>
        {
            // digit,  color,  multiplier, tolerance = 0
            new ResistorBand(0, "black", (1 * Math.Pow(10, 0))),
            new ResistorBand(1, "brown", (1 * Math.Pow(10, 1)), 0.01),
            new ResistorBand(2, "red",  (1 * Math.Pow(10, 2)), 0.02),
            new ResistorBand(3, "orange", (1 * Math.Pow(10, 3))),
            new ResistorBand(4, "yellow", (1 * Math.Pow(10, 4)), 0.05),
            new ResistorBand(5, "green", (1 * Math.Pow(10, 5)), 0.005),
            new ResistorBand(6, "blue", (1 * Math.Pow(10, 6)), 0.0025),
            new ResistorBand(7, "violet",(1 * Math.Pow(10, 7)), 0.001),
            new ResistorBand(8, "gray", (1 * Math.Pow(10, 8)), 0.0005),
            new ResistorBand(9, "white", (1 * Math.Pow(10, 9))),
            new ResistorBand{Color = "pink", Multiplier = (1 * Math.Pow(10, -3))},
            new ResistorBand{Color = "silver", Multiplier = (1 * Math.Pow(10, -2)), Tolerance = 0.1},
            new ResistorBand{Color = "gold", Multiplier = (1 * Math.Pow(10, -1)),  Tolerance = .05},
        };
        
        public int CalculateOhmValue(string bandAColor, string bandBColor, string bandCColor, string bandDColor = "none")
        {
            var a = ResistorBands.Find(band => band.Color == bandAColor).Digit;
            var b = ResistorBands.Find(band => band.Color == bandBColor).Digit; 
            var c = ResistorBands.Find(band => band.Color == bandCColor).Multiplier;
            var d = bandDColor == "none"? 0 : ResistorBands.Find(band => band.Color == bandDColor).Tolerance;

            int result = 0;

            try
            {
                result = checked((int) (((a * 10 + b) * (int) c)));
            }
            catch (System.OverflowException e)
            {
                Console.WriteLine("Overflow caught" + e.ToString());
            }

            return result;

        }
        
        public double GetTolerance(string bandDColor)
        {
            var bandTolerance = bandDColor == "none" ? .20 : ResistorBands.Find(band => band.Color == bandDColor).Tolerance;
            return bandTolerance;
        }

    }
}