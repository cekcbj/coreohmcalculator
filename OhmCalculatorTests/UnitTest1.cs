using System;
using OhmCalculator.Models;
using Xunit;

namespace OhmCalculatorTests
{
    public class OhmValueCalculatorTests
    {

        readonly OhmValueCalculator calculator = new OhmValueCalculator();

        [Fact]
        public void Calculate_Method_Returns_Int()
        {
            var value = calculator.CalculateOhmValue("yellow", "blue", "black", "white");
            Assert.IsType<int>(value);
        }
        
        [Fact]
        public void Calculate_Returns_CorrectValue()
        {
            var value1 = calculator.CalculateOhmValue("yellow", "blue", "black");
            var value2 = calculator.CalculateOhmValue("red", "white", "blue", "gold");
            var value3 = calculator.CalculateOhmValue("green", "violet", "yellow", "red");
            var value4 = calculator.CalculateOhmValue("brown", "gray", "orange", "silver");
            Assert.Equal(46, value1);
            Assert.Equal(29000000, value2);
            Assert.Equal(570000 , value3);
            Assert.Equal(18000, value4);
        }
    }

}
